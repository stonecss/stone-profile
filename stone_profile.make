core = 7.x
api = 2


; DEFAULTS =====================================================================

defaults[projects][subdir] = contrib


; MODULES ======================================================================

; System
projects[stone_access][type] = module
projects[stone_access][subdir] = system
projects[stone_access][download][type] = git
projects[stone_access][download][url] = git@bitbucket.org:stonecss/stone-access.git
projects[stone_access][download][branch] = 7.x-1.x

projects[stone_admin][type] = module
projects[stone_admin][subdir] = system
projects[stone_admin][download][type] = git
projects[stone_admin][download][url] = git@bitbucket.org:stonecss/stone-admin.git
projects[stone_admin][download][branch] = 7.x-1.x

projects[stone_cookies][type] = module
projects[stone_cookies][subdir] = system
projects[stone_cookies][download][type] = git
projects[stone_cookies][download][url] = git@bitbucket.org:stonecss/stone-cookies.git
projects[stone_cookies][download][branch] = 7.x-1.x

projects[stone_core][type] = module
projects[stone_core][subdir] = system
projects[stone_core][download][type] = git
projects[stone_core][download][url] = git@bitbucket.org:stonecss/stone-core.git
projects[stone_core][download][branch] = 7.x-1.x


projects[stone_machine_name][type] = module
projects[stone_machine_name][subdir] = system
projects[stone_machine_name][download][type] = git
projects[stone_machine_name][download][url] = git@bitbucket.org:stonecss/stone-machine-name.git
projects[stone_machine_name][download][branch] = 7.x-1.x

projects[stone_mail][type] = module
projects[stone_mail][subdir] = system
projects[stone_mail][download][type] = git
projects[stone_mail][download][url] = git@bitbucket.org:stonecss/stone-mail.git
projects[stone_mail][download][branch] = 7.x-1.x

projects[stone_pages][type] = module
projects[stone_pages][subdir] = system
projects[stone_pages][download][type] = git
projects[stone_pages][download][url] = git@bitbucket.org:stonecss/stone-pages.git
projects[stone_pages][download][branch] = 7.x-1.x

projects[stone_panels][type] = module
projects[stone_panels][subdir] = system
projects[stone_panels][download][type] = git
projects[stone_panels][download][url] = git@bitbucket.org:stonecss/stone-panels.git
projects[stone_panels][download][branch] = 7.x-1.x

projects[stone_wysiwyg][type] = module
projects[stone_wysiwyg][subdir] = system
projects[stone_wysiwyg][download][type] = git
projects[stone_wysiwyg][download][url] = git@bitbucket.org:stonecss/stone-wysiwyg.git
projects[stone_wysiwyg][download][branch] = 7.x-1.x

; Contrib
projects[addressfield][version] = 1.0-rc1
projects[addressfield_tokens][version] = 1.5
projects[admin_menu][version] = 3.0-rc5
projects[advagg][version] = 2.7
projects[better_formats][version] = 1.0-beta1
projects[cdn][version] = 2.6
projects[ctools][version] = 1.5
projects[date][version] = 2.8
projects[devel][version] = 1.5
projects[diff][version] = 3.2
projects[draggableviews][version] = 2.0
projects[elements][version] = 1.4
projects[email][version] = 1.3
projects[email_registration][version] = 1.2
projects[emogrifier][version] = 2.0-beta1
projects[entity][version] = 1.5
projects[entityreference][version] = 1.1
projects[features][version] = 2.3
projects[field_group][version] = 1.4
projects[file_entity][version] = 2.0-beta1
projects[find_content][version] = 1.4
projects[geocoder][version] = 1.2
projects[geofield][version] = 2.1
projects[geophp][version] = 1.7
projects[globalredirect][version] = 1.5
projects[google_analytics][version] = 2.1
projects[htmlmail][version] = 2.65
projects[jquery_update][version] = 2.4
projects[libraries][version] = 2.2
projects[link][version] = 1.3
projects[linkit][version] = 3.3
projects[mailsystem][version] = 2.34
projects[media][version] = 2.0-alpha4
projects[menu_admin_per_menu][version] = 1.0
projects[menu_block][version] = 2.4
projects[menu_node][version] = 1.2
projects[menu_position][version] = 1.1
projects[metatag][version] = 1.4
projects[migrate][version] = 2.6
projects[migrate_extras][version] = 2.5
projects[module_filter][version] = 2.0-alpha2
projects[office_hours][version] = 1.4
projects[panelizer][version] = 3.1
projects[panels][version] = 3.4
projects[panels_everywhere][version] = 1.0-rc1
projects[pathauto][version] = 1.2
projects[pathauto_persist][version] = 1.3
projects[pathologic][version] = 2.12
projects[redirect][version] = 1.0-rc1
projects[reroute_email][version] = 1.2
projects[role_delegation][version] = 1.1
projects[rules][version] = 2.7
projects[site_map][version] = 1.2
projects[smtp][version] = 1.2
projects[strongarm][version] = 2.0
projects[token][version] = 1.5
projects[transliteration][version] = 3.2
projects[variable][version] = 2.5
projects[views][version] = 3.8
projects[views_bulk_operations][version] = 3.2
projects[webform][version] = 4.2
projects[webform_validation][version] = 1.7
projects[wysiwyg][version] = 2.x-dev
projects[wysiwyg_filter][version] = 1.6-rc2
projects[xmlsitemap][version] = 2.1

; Internationalisation
projects[countries][version] = 2.3
projects[entity_translation][version] = 1.0-beta3
projects[entity_translation][patch][1925848] = http://www.drupal.org/files/entitytranslation-incorrect_pathauto_pattern-1925848-8.patch
projects[i18n][version] = 1.11
projects[pathauto_i18n][version] = 1.3
projects[title][version] = 1.0-alpha7

; Transitional - to be added to Drupal in future
projects[cssnext_prepro][type] = module
projects[cssnext_prepro][download][type] = git
projects[cssnext_prepro][download][url] = git@bitbucket.org:stonecss/cssnext-prepro.git
projects[cssnext_prepro][download][branch] = 7.x-1.x

projects[custom_prepro][type] = module
projects[custom_prepro][download][type] = git
projects[custom_prepro][download][url] = git@bitbucket.org:stonecss/custom-prepro.git
projects[custom_prepro][download][branch] = 7.x-1.x

; THEMES =======================================================================

projects[stone][type] = theme
projects[stone][subdir] = system
projects[stone][download][type] = git
projects[stone][download][url] = git@bitbucket.org:stonecss/stone.git
projects[stone][download][branch] = 7.x-1.x


; LIBRARIES ====================================================================

projects[ckeditor][type] = library
projects[ckeditor][download][type] = git
projects[ckeditor][download][url] = https://github.com/ckeditor/ckeditor-releases.git
projects[ckeditor][download][tag] = full/4.4.6
projects[ckeditor][subdir] = ""

projects[cssnext_php_cli][type] = library
projects[cssnext_php_cli][download][type] = git
projects[cssnext_php_cli][download][url] = git@bitbucket.org:stonecss/cssnext-php-cli.git
projects[cssnext_php_cli][download][tag] = 0.1.1
projects[cssnext_php_cli][subdir] = ""

projects[cycle2][type] = library
projects[cycle2][download][type] = file
projects[cycle2][download][url] = http://malsup.github.com/min/jquery.cycle2.min.js
projects[cycle2][subdir] = ""

projects[cycle2.map][type] = library
projects[cycle2.map][download][type] = file
projects[cycle2.map][download][url] = http://malsup.github.io/min/jquery.cycle2.js.map
projects[cycle2.map][subdir] = ""
projects[cycle2.map][directory_name] = "cycle2"

projects[cycle2.swipe][type] = library
projects[cycle2.swipe][download][type] = file
projects[cycle2.swipe][download][url] = http://malsup.github.io/min/jquery.cycle2.swipe.min.js
projects[cycle2.swipe][subdir] = ""

projects[emogrifier_library][type] = library
projects[emogrifier_library][directory_name] = emogrifier
projects[emogrifier_library][download][type] = git
projects[emogrifier_library][download][url] = https://github.com/jjriv/emogrifier.git
projects[emogrifier_library][download][tag] = v1.0alpha1
projects[emogrifier_library][subdir] = ""

projects[jquery-placeholder][type] = library
projects[jquery-placeholder][download][type] = git
projects[jquery-placeholder][download][url] = https://github.com/mathiasbynens/jquery-placeholder.git
projects[jquery-placeholder][download][tag] = v2.1.0
projects[jquery-placeholder][subdir] = ""

projects[selecter][type] = library
projects[selecter][download][type] = git
projects[selecter][download][url] = https://github.com/benplum/Selecter.git
projects[selecter][download][tag] = 3.2.4
projects[selecter][subdir] = ""

projects[selectivizr][type] = library
projects[selectivizr][download][type] = git
projects[selectivizr][download][url] = https://github.com/keithclark/selectivizr.git
projects[selectivizr][download][tag] = 1.0.2
projects[selectivizr][subdir] = ""
